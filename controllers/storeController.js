// exports.myMiddleWare = (req, res, next) => {
//     req.name = 'Wes';
//     // res.cookie('name', 'wes is cool', { maxAge: 9000000})
//     if(req.name == 'Wes') {
//         throw Error('That is a NOT stupid name');
//     }
//     next(); 
// }

const mongoose = require('mongoose');
const Store = mongoose.model('Store');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req, file, next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto) {
            // null is when its not an image and true if its an image
            next(null, true);
        } else {
            next({message: 'That filetype isn\'t allowed!'}, false);
        }
    }
};

exports.homePage = (req, res) => {
    res.render('index');
};

exports.addStore = (req, res) => {
    res.render('editStore', {title: 'Add Store'});
};

exports.upload = multer(multerOptions).single('photo');

exports.resize = async (req, res, next) => {
    // check if there is no new file to resize
    if(!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.photo = `${uuid.v4()}.${extension}`;

    // now we resize the photo
    const photo = await jimp.read(req.file.buffer);
    await photo.resize(800, jimp.AUTO);

    //write it to the actual folder
    await photo.write(`./public/uploads/${req.body.photo}`);

    // once we have written the photo to our filesystem, keep going!
    next();
}

exports.createStore = async (req, res) => {
    const store = await (new Store(req.body)).save();
    req.flash('success', `Successfully created ${store.name}. Care to leave a review?`);
    res.redirect(`/store/${store.slug}`);
};

exports.getStores = async (req, res) => {
    // 1. query the database for a list of all stores
    const stores = await Store.find();
    
    res.render('stores', {title: 'Stores', stores });
};

exports.editStore = async (req, res) => {
    // 1. find the store given to the id
    const store = await Store.findOne({ _id: req.params.id });
    
    // 2. confirm they are the owner of the store

    // 3. render out the edit form so the user can update their store
    res.render('editStore', { title: `Edit ${store.name}`, store })
};

exports.updateStore = async (req, res) => {

    // Set the location data to be a point
    req.body.location.type = "Point";

    // 1. find and update the store
    const store = await Store.findOneAndUpdate({ _id: req.params.id }, req.body, {
        new: true, // return the new store instead of the new one
        runValidators: true
    }).exec();

    req.flash('success', `Successfully updated <strong>${store.name}</strong>. <a href="/stores/${store.slug}">View Store</a>`);
    res.redirect(`/stores/${store._id}/edit`);
    // 2. redirect them to the store and tell it worked

};

exports.getStoreBySlug = async (req, res, next) => {
    const store = await Store.findOne({ slug: req.params.slug });
    if (!store) return next();    

    res.render('store', { title: `${store.name}`, store });
}

exports.getStoreByTag = async (req, res) => {
    // res.send('it works!');
    // res.json(stores);
    const tag = req.params.tag;
    const tagQuery = tag || { $exists: true };
    const tagsPromise = Store.getTagList();
    const storesPromise = Store.find({ tags: tagQuery });
    const [tags, stores] = Promise.all([tagsPromise, storesPromise])
    // res.render('tag', { tags, title: 'Tags', tag, stores });
}