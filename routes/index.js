const express = require('express');
const router = express.Router();
const storeController = require('../controllers/storeController')

const { catchErrors } = require('../handlers/errorHandlers'); 

// Do work here
// router.get('/', (req, res) => {
  // res.send('Hey! How about now?!');
//   res.render('hello', {
//     name: req.query.name,
//     cool: true,
//     dog: req.query.dog, 
//     title: 'I love food'
//   });
// });

// router.get('/', storeController.myMiddleWare, storeController.homePage);

router.get('/', catchErrors(storeController.getStores));
router.get('/stores', catchErrors(storeController.getStores));
router.get('/add', storeController.addStore);

router.post('/add', 
  storeController.upload, 
  catchErrors(storeController.resize),
  catchErrors(storeController.createStore)
);

router.post('/add/:id', 
  storeController.upload, 
  catchErrors(storeController.resize),
  catchErrors(storeController.updateStore)
);

router.get('/stores/:id/edit', catchErrors(storeController.editStore));

router.get('/store/:slug', catchErrors(storeController.getStoreBySlug));

// means the tag is optional
// router.get('/tags/:tag*?', catchErrors(storeController.getStoreByTag));

router.get('/tags', storeController.getStoreByTag);
router.get('/tags/:tag', storeController.getStoreByTag);

module.exports = router;
